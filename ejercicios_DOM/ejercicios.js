//Ejercicio1
function mayor(a, b) {
   if (a > b) {
      return (a + " es mayor que " + b);
   } else if (a == b) {
      return (a + " y " + b + " son iguales.");
   } else {
      return (b + " es mayor que " + a);
   }
}
//Ejercicio2
let par_impar;
let div_3;
let div_5;
let div_7;

function datos(x) {
   if (x % 2 == 0) {
      par_impar = (x + " es par.");
   } else {
      par_impar = (x + " es impar.");
   }
   if (x % 3 == 0) {
      div_3 = ("SI es divisible por " + 3);
   } else {
      div_3 = ("NO es divisible por " + 3);
   }
   if (x % 5 == 0) {
      div_5 = ("SI es divisible por " + 5);
   } else {
      div_5 = ("NO es divisible por " + 5);
   }
   if (x % 7 == 0) {
      div_7 = ("SI es divisible por " + 7);
   } else {
      div_7 = ("NO es divisible por " + 7);
   }
}
//Ejercicio3
function sumaValores([...arr]) {
   let total = 0;
   for (pos in arr) {
      total = total + arr[pos];
   }
   return total;
}
//Ejercicio4
function factorial(x) {
   if (x >= 1) {
      return x * factorial(x - 1);
   } else {
      return 1;
   }
}
//Ejercicio5
function primo(x) {
   if (x == 1 || x == 0) {
      return "NO es primo.";
   }
   for (i = 2; i <= x / 2; i++) {
      if (x % i == 0) {
         return "NO es primo.";
      }
   }
   return "SI es primo.";
}
//Ejercicio6
function fibonacci(x) {
   if (x == 0 || x == 1) {
      return [0, 1];
   } else {
      let serie = fibonacci(x - 1);
      serie.push(serie[serie.length - 1] + serie[serie.length - 2])
      return serie;
   }
}
//Ejercicio7


//Ejercicio8
function capitaliza(frase) {
   return (frase.charAt(0).toUpperCase() + frase.slice(1));
}
//Ejercicio9
function palabra(plbr) {
   length = plbr.length;
   if (length % 2 == 0) {
      palabraPar = "Es par."
   } else {
      palabraPar = "Es impar"

   }
}